package com.example.leaderboard.data.remote

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.example.leaderboard.data.remote.servies.LeaderBoardService
import com.example.leaderboard.model.Store
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class LeaderBoardRepository {
    private val client = NetworkRestClient.createNetworkClient()
    private var leaderBoardService: LeaderBoardService
    var storeList = MediatorLiveData<ArrayList<Store>>()
    val errorLiveData = MutableLiveData<Boolean>()

    init {
        leaderBoardService = client.create(LeaderBoardService::class.java)
    }

    //fetch from service
    fun fetchStores() {
        val apps = leaderBoardService.getStores()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                storeList.value?.addAll(it.apps)
                storeList.postValue(it.apps)
                errorLiveData.postValue(false)
            }, { error ->
                println("Error: $error")
                errorLiveData.postValue(false)
            }, {
                println("Completed")
            })
    }

    //get list
    fun getStoreLiveData(): MediatorLiveData<ArrayList<Store>> {
        return storeList
    }
}