package com.example.leaderboard.data.remote.servies

import com.example.leaderboard.model.Apps
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
interface LeaderBoardService {
    @GET("/dummy-app-data.json")
    fun getStores(): Observable<Apps>
}