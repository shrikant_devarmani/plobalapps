package com.example.leaderboard.data.remote

import com.example.leaderboard.helper.AppConstants.Companion.BASE_URL
import com.example.leaderboard.helper.AppConstants.Companion.CONNECT_TIMEOUT
import com.example.leaderboard.helper.AppConstants.Companion.READ_TIMEOUT
import com.example.leaderboard.helper.AppConstants.Companion.WRITE_TIMEOUT
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class NetworkRestClient {
    companion object {
        fun createNetworkClient(): Retrofit {
            val client = OkHttpClient.Builder();
            client.apply {
                connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                retryOnConnectionFailure(true)

//                retrofit.create(UserService::class.java)

                return Retrofit.Builder()
                    .client(client.build())
                    .addConverterFactory(
                        GsonConverterFactory.create()
                    )
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
            }
        }
    }
}