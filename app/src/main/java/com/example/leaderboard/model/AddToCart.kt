package com.example.leaderboard.model

import com.google.gson.annotations.SerializedName

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
data class AddToCart(var total: Long, @SerializedName("month_wise") var monthWise: MonthWise)