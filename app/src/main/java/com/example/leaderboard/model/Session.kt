package com.example.leaderboard.model

import com.google.gson.annotations.SerializedName

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
data class Session(var total: Int, @SerializedName("month_wise")  var monthWise: MonthWise, var monthList:ArrayList<String>)