package com.example.leaderboard.model

import java.io.Serializable

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
data class Store(var name: String, var currency: String, var money_format: String, var data: Data) :
    Serializable