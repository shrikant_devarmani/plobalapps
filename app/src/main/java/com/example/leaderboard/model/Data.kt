package com.example.leaderboard.model

import com.google.gson.annotations.SerializedName

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
data class Data(
    var downloads: Downloads,
    var sessions: Session,
    @SerializedName("total_sale") var totalSales: TotalSales,
    @SerializedName("add_to_cart") var addToCart: AddToCart
)