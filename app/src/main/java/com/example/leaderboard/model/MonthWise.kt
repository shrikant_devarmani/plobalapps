package com.example.leaderboard.model

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
data class MonthWise(
    var jan: Int,
    var feb: Int,
    var mar: Int,
    var apr: Int,
    var may: Int,
    var jun: Int
)