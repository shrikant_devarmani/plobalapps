package com.example.leaderboard.helper

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import com.google.android.material.snackbar.Snackbar

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class CorUtility {

    companion object {
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun showSnackbar(
            view: View,
            message: String,
            action: String,
            duration: Int = Snackbar.LENGTH_INDEFINITE
        ) {
            val snackbar = Snackbar.make(view, message, duration)
            snackbar.setAction(action) { v: View -> snackbar.dismiss() }
            snackbar.show()
        }
    }
}



