package com.example.leaderboard.helper

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
abstract class BaseActivity : AppCompatActivity(), ViewModelStoreOwner {

    private lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleRegistry = LifecycleRegistry(this)
    }

    override fun getViewModelStore(): ViewModelStore {
        return ViewModelStore()
    }

    abstract fun initComponent()
}