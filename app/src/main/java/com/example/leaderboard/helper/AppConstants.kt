package com.example.leaderboard.helper

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
open class AppConstants {
    companion object {
        const val BASE_URL = "https://plobalapps.s3-ap-southeast-1.amazonaws.com"
        const val CONNECT_TIMEOUT = 10L
        const val WRITE_TIMEOUT = 1L
        const val READ_TIMEOUT = 20L
    }
}