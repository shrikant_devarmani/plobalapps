package com.example.leaderboard.helper;

import com.example.leaderboard.model.Store;

import java.util.ArrayList;

/**
 * Quick sort took from https://www.vogella.com/tutorials/JavaAlgorithmsQuicksort/article.html
 */
public class Quicksort {
    private ArrayList<Store> numbers;
    private ArrayList<Store> sortedList = new ArrayList<>();

    public ArrayList<Store> sort(ArrayList<Store> values) {
        // check for empty or null array
        if (values == null || values.size() == 0) {
            return new ArrayList<>();
        }
        this.numbers = values;
        int number = values.size();
        return quicksort(0, number - 1);
    }

    private ArrayList<Store> quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        Store pivot = numbers.get(low + (high - low) / 2);

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            while (numbers.get(i).getData().getTotalSales().getTotal() < pivot.getData().getTotalSales().getTotal()) {
                i++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (numbers.get(j).getData().getTotalSales().getTotal() > pivot.getData().getTotalSales().getTotal()) {
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);

        return sortedList;
    }

    private void exchange(int i, int j) {
        Store temp = numbers.get(i);
        numbers.set(i, numbers.get(j));
        numbers.set(j, temp);
        sortedList.clear();
        sortedList.addAll(numbers);
    }
}