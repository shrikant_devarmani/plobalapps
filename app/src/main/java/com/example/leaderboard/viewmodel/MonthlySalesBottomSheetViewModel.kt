package com.example.leaderboard.viewmodel

import androidx.lifecycle.ViewModel
import com.example.leaderboard.model.Store
import lecho.lib.hellocharts.model.AxisValue
import lecho.lib.hellocharts.model.LineChartData
import lecho.lib.hellocharts.model.PointValue

class MonthlySalesBottomSheetViewModel : ViewModel() {
    /**
     * Values are same so no problem (4 options)
     */
    fun getAxisXLabels(store: Store): ArrayList<AxisValue> {
        val mAxisXValues = ArrayList<AxisValue>()
        for (i in 0 until 5) {
            when (i) {
                0 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.jan.toFloat()).setLabel(
                            "Jan"
                        )
                    )
                }
                1 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.feb.toFloat()).setLabel(
                            "feb"
                        )
                    )
                }
                2 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.mar.toFloat()).setLabel(
                            "Mar"
                        )
                    )
                }
                3 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.apr.toFloat()).setLabel(
                            "Apr"
                        )
                    )
                }
                4 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.may.toFloat()).setLabel(
                            "may"
                        )
                    )
                }
                5 -> {
                    mAxisXValues.add(
                        AxisValue(store.data.sessions.monthWise.jun.toFloat()).setLabel(
                            "Jun"
                        )
                    )
                }
            }
        }
        return mAxisXValues
    }

    fun prepareDataAnimation(data: LineChartData) {
        for (line in data.lines) {
            for (value in line.values) {
                // Here I modify target only for Y values but it is OK to modify X targets as well.
                value.setTarget(value.x, value.y)
            }
        }
    }

    fun getTotalSalesData(store: Store): ArrayList<PointValue> {
        val entries: MutableList<PointValue> = ArrayList()
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.jan.toFloat(),
                store.data.totalSales.monthWise.feb.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.feb.toFloat(),
                store.data.totalSales.monthWise.mar.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.mar.toFloat(),
                store.data.totalSales.monthWise.apr.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.apr.toFloat(),
                store.data.totalSales.monthWise.may.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.may.toFloat(),
                store.data.totalSales.monthWise.jun.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.totalSales.monthWise.jun.toFloat(),
                store.data.totalSales.monthWise.jun.toFloat()
            )
        )
        return entries as ArrayList<PointValue>
    }

    fun getCartData(store: Store): ArrayList<PointValue> {
        val entries: MutableList<PointValue> = ArrayList()
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.jan.toFloat(),
                store.data.addToCart.monthWise.feb.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.feb.toFloat(),
                store.data.addToCart.monthWise.mar.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.mar.toFloat(),
                store.data.addToCart.monthWise.apr.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.apr.toFloat(),
                store.data.addToCart.monthWise.may.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.may.toFloat(),
                store.data.addToCart.monthWise.jun.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.addToCart.monthWise.jun.toFloat(),
                store.data.addToCart.monthWise.jun.toFloat()
            )
        )
        return entries as ArrayList<PointValue>
    }

    fun getDownloadData(store: Store): ArrayList<PointValue> {
        val entries: MutableList<PointValue> = ArrayList()
        entries.add(
            PointValue(
                store.data.downloads.monthWise.jan.toFloat(),
                store.data.downloads.monthWise.feb.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.downloads.monthWise.feb.toFloat(),
                store.data.downloads.monthWise.mar.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.downloads.monthWise.mar.toFloat(),
                store.data.downloads.monthWise.apr.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.downloads.monthWise.apr.toFloat(),
                store.data.downloads.monthWise.may.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.downloads.monthWise.may.toFloat(),
                store.data.downloads.monthWise.jun.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.downloads.monthWise.jun.toFloat(),
                store.data.downloads.monthWise.jun.toFloat()
            )
        )
        return entries as ArrayList<PointValue>
    }

    fun getUserSessionData(store: Store): ArrayList<PointValue> {
        val entries: MutableList<PointValue> = ArrayList()
        entries.add(
            PointValue(
                store.data.sessions.monthWise.jan.toFloat(),
                store.data.sessions.monthWise.feb.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.sessions.monthWise.feb.toFloat(),
                store.data.sessions.monthWise.mar.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.sessions.monthWise.mar.toFloat(),
                store.data.sessions.monthWise.apr.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.sessions.monthWise.apr.toFloat(),
                store.data.sessions.monthWise.may.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.sessions.monthWise.may.toFloat(),
                store.data.sessions.monthWise.jun.toFloat()
            )
        )
        entries.add(
            PointValue(
                store.data.sessions.monthWise.jun.toFloat(),
                store.data.sessions.monthWise.jun.toFloat()
            )
        )
        return entries as ArrayList<PointValue>
    }
}