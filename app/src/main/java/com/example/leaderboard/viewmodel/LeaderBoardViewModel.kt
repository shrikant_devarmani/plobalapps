package com.example.leaderboard.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.leaderboard.data.remote.LeaderBoardRepository
import com.example.leaderboard.model.Store

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class LeaderBoardViewModel : ViewModel() {
    private val repository = LeaderBoardRepository()
    var storeList: LiveData<ArrayList<Store>>
    var loading = MutableLiveData<Boolean>()

    init {
        storeList = repository.getStoreLiveData()
        loading = repository.errorLiveData
        getUsers()
    }

    private fun getUsers() {
        loading.postValue(true)
        repository.fetchStores()
    }
}