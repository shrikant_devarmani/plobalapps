package com.example.leaderboard.view.leaderboard

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
interface OnCardClickListener {
    abstract fun <T> onItemClick(`object`: T, position: Int)
}