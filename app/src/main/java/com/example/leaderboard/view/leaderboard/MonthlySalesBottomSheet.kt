package com.example.leaderboard.view.leaderboard

import android.app.Activity
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.leaderboard.R
import com.example.leaderboard.databinding.BottomsheetMonthlySalesBinding
import com.example.leaderboard.model.Store
import com.example.leaderboard.viewmodel.MonthlySalesBottomSheetViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import lecho.lib.hellocharts.gesture.ContainerScrollType
import lecho.lib.hellocharts.gesture.ZoomType
import lecho.lib.hellocharts.model.Axis
import lecho.lib.hellocharts.model.Line
import lecho.lib.hellocharts.model.LineChartData
import lecho.lib.hellocharts.model.PointValue


/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 * @help  //https://programming.vip/docs/hellocharts-android-chart-library.html
 */
class MonthlySalesBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding: BottomsheetMonthlySalesBinding
    private lateinit var monthlySalesVM: MonthlySalesBottomSheetViewModel
    private var storeBundleValue: Store? = null
    private lateinit var animation: Animation
    private lateinit var currency: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_monthly_sales, null, false)
        binding.rootContainer.setBackgroundColor(Color.TRANSPARENT)
        animation = AnimationUtils.loadAnimation(requireContext(), R.anim.alpha)
        monthlySalesVM =
            ViewModelProvider(this).get(MonthlySalesBottomSheetViewModel::class.java)

        binding.apply {
            monthySalesBottomSheetVM = monthlySalesVM
        }

        val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(binding.rootContainer)
        val layoutParams = binding.rootContainer.layoutParams

        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                layoutParams.height = windowHeight
            } else {
                layoutParams.height = windowHeight / 2
            }
        }
        binding.rootContainer.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED

        storeBundleValue = arguments?.getSerializable("STORE") as Store
        binding.companySummaryLayout.tvCompanyName.text = storeBundleValue?.name
        currency = storeBundleValue?.currency.toString() + " "
        binding.companySummaryLayout.tvTotalSalesValue.text =
            currency + storeBundleValue?.data?.totalSales?.total.toString()


        prepareLineDataSet(storeBundleValue!!)

        return binding.root
    }

    /**
     * Line chart prepration
     */
    private fun prepareLineDataSet(store: Store) {
        var values = monthlySalesVM.getTotalSalesData(store)

        binding.monthlySalesChart.isInteractive = true
        binding.monthlySalesChart.zoomType = ZoomType.HORIZONTAL_AND_VERTICAL
        binding.monthlySalesChart.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL)

        val data = LineChartData()
        invalidateView(values, store, data)

        binding.ivCurrency.setOnClickListener {
            binding.companySummaryLayout.tvTotalSalesValue.text =
                currency + storeBundleValue?.data?.totalSales?.total.toString()
            binding.ivCurrency.startAnimation(animation)
            values = monthlySalesVM.getTotalSalesData(store)
            invalidateView(values, store, data)
        }

        binding.ivCart.setOnClickListener {
            binding.companySummaryLayout.tvTotalSalesValue.text =
                currency + storeBundleValue?.data?.addToCart?.total.toString()

            binding.ivCart.startAnimation(animation)
            values = monthlySalesVM.getCartData(store)
            invalidateView(values, store, data)
        }

        binding.ivDownloads.setOnClickListener {
            binding.companySummaryLayout.tvTotalSalesValue.text =
                currency + storeBundleValue?.data?.downloads?.total.toString()
            binding.ivDownloads.startAnimation(animation)
            values = monthlySalesVM.getDownloadData(store)
            invalidateView(values, store, data)
        }
        binding.ivUserSession.setOnClickListener {
            binding.companySummaryLayout.tvTotalSalesValue.text =
                currency + storeBundleValue?.data?.sessions?.total.toString()
            binding.ivUserSession.startAnimation(animation)
            values = monthlySalesVM.getUserSessionData(store)
            invalidateView(values, store, data)
        }
    }

    /**
     * Set new data on clic of images options like cart, download, currency, user sessions
     */
    private fun invalidateView(
        values: ArrayList<PointValue>,
        store: Store,
        data: LineChartData
    ) {
        //In most cased you can call data model methods in builder-pattern-like manner.
        val line: Line =
            Line(values).setColor(ContextCompat.getColor(requireContext(), R.color.grey_400))
                .setCubic(true)
        line.isFilled = true
        //line.setHasLabels(true)

        //Axis of coordinates
        val axisX = Axis() //X axis
        axisX.textColor = Color.GRAY
        axisX.textSize = 10
        axisX.maxLabelChars = 3
        axisX.values = monthlySalesVM.getAxisXLabels(store)

        //axisX.setHasLines(true)

        val lines: MutableList<Line> = ArrayList()
        lines.add(line)

        data.axisXBottom = axisX
        data.lines = lines

        binding.monthlySalesChart.lineChartData = data
        binding.monthlySalesChart.invalidate()
        animateChart(data)
    }

    private fun animateChart(data: LineChartData) {
        monthlySalesVM.prepareDataAnimation(data)
        binding.monthlySalesChart.startDataAnimation(5000)
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay
            .getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }
}