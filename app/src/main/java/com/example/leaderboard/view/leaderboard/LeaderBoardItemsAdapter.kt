package com.example.leaderboard.view.leaderboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.leaderboard.R
import com.example.leaderboard.databinding.CardLeaderboardItemsLayoutBinding
import com.example.leaderboard.model.Store
import kotlinx.android.synthetic.main.card_leaderboard_items_layout.view.*

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class LeaderBoardItemsAdapter(
    var storeItemList: ArrayList<Store>,
    var listener: OnCardClickListener
) : RecyclerView.Adapter<LeaderBoardItemsAdapter.LeaderBoardItemHolder>() {
    private lateinit var binding: CardLeaderboardItemsLayoutBinding

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LeaderBoardItemHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.card_leaderboard_items_layout,
            parent, false
        )
        return LeaderBoardItemHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return storeItemList.size
    }

    override fun onBindViewHolder(holder: LeaderBoardItemHolder, position: Int) {
        val dataModel = storeItemList[position]
        holder.name.text = dataModel.name
        holder.totalSales.text =
            dataModel.currency + " " + dataModel.data.totalSales.total.toString()
    }

    /**
     * Data holder class
     */
    inner class LeaderBoardItemHolder(binding: View) :
        RecyclerView.ViewHolder(binding) {
        val name = binding.tvCompanyName
        val totalSales = binding.tvTotalSalesValue

        init {
            binding.setOnClickListener {
                listener.onItemClick(storeItemList[adapterPosition], adapterPosition)
            }
        }
    }
}