package com.example.leaderboard.view.leaderboard

import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.leaderboard.R
import com.example.leaderboard.databinding.BottomsheetSortByBinding
import com.example.leaderboard.helper.CorUtility
import com.example.leaderboard.helper.Quicksort
import com.example.leaderboard.model.Store
import com.example.leaderboard.viewmodel.SortByBottomSheetViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class SortByBottomSheet(val listener: DialogListener) : BottomSheetDialogFragment() {

    private lateinit var binding: BottomsheetSortByBinding
    private lateinit var sortByBottomSheetViewModel: SortByBottomSheetViewModel
    private var storeBundleValue: ArrayList<Store>? = null
    private var sortClick = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_sort_by, null, false)
        sortByBottomSheetViewModel =
            ViewModelProvider(this).get(SortByBottomSheetViewModel::class.java)

        val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(binding.rootContainer)
        val layoutParams = binding.rootContainer.layoutParams

        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                layoutParams.height = windowHeight
            } else {
                layoutParams.height = windowHeight / (2.5).toInt()
            }
        }
        binding.rootContainer.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED

        storeBundleValue = arguments?.getSerializable("STORE_LIST") as ArrayList<Store>
        sortClick = arguments?.getInt("SORT")!!

        //todo need to add code for other options
        if (sortClick == 1) {
            binding.rbTotalSales.setButtonDrawable(R.drawable.check)
        }

        setListenersOnView()

        return binding.root
    }

    private fun setListenersOnView() {
        binding.rbGroup.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rbTotalSales) {
                binding.rbTotalSales.setButtonDrawable(R.drawable.check)
                getSortingValues(storeBundleValue!!)
            }

            if (checkedId == R.id.rbAddToCart) {
//                binding.rbAddToCart.setButtonDrawable(R.drawable.check)
                CorUtility.showSnackbar(
                    binding.rootContainer,
                    getString(R.string.need_to_implement),
                    getString(R.string.dismiss)
                )
            }

            if (checkedId == R.id.rbSession) {
//                binding.rbSession.setButtonDrawable(R.drawable.check)
                CorUtility.showSnackbar(
                    binding.rootContainer,
                    getString(R.string.need_to_implement),
                    getString(R.string.dismiss)
                )
            }

            if (checkedId == R.id.rbDownloads) {
//                binding.rbDownloads.setButtonDrawable(R.drawable.check)
                CorUtility.showSnackbar(
                    binding.rootContainer,
                    getString(R.string.need_to_implement),
                    getString(R.string.dismiss)
                )
            }
        }
    }

    /**
     * todo : add sort method for other options remaining
     * This sort method works for only Total Sales first option
     */
    private fun getSortingValues(list: ArrayList<Store>) {
        val data = Quicksort().sort(list)
        listener.onItemSelected(data)
        dismiss()
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay
            .getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    interface DialogListener {
        fun onItemSelected(list: ArrayList<Store>)
    }
}


