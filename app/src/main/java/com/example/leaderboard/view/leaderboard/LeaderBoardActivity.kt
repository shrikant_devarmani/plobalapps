package com.example.leaderboard.view.leaderboard

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.leaderboard.R
import com.example.leaderboard.databinding.ActivityLeaderBoardBinding
import com.example.leaderboard.helper.BaseActivity
import com.example.leaderboard.helper.CorUtility
import com.example.leaderboard.model.Store
import com.example.leaderboard.viewmodel.LeaderBoardViewModel
import kotlinx.android.synthetic.main.header.view.*

/**
 * @createdby Shrikant Devarmani
 * @date 7/25/2020
 */
class LeaderBoardActivity : BaseActivity(), OnCardClickListener, SortByBottomSheet.DialogListener {
    private lateinit var binding: ActivityLeaderBoardBinding
    private lateinit var leaderBoardViewModel: LeaderBoardViewModel
    private lateinit var leaderBoardItemsAdapter: LeaderBoardItemsAdapter
    private var list = ArrayList<Store>()
    private var sortClick = 0
    private var bottomSheetFragment: SortByBottomSheet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        binding = DataBindingUtil.setContentView(this, R.layout.activity_leader_board)
        binding.apply {
            lifecycleOwner = this@LeaderBoardActivity
            leaderBoardViewModel = this.leaderBoardViewModel
        }

        initComponent()
    }

    override fun initComponent() {
        leaderBoardViewModel = ViewModelProvider(this).get(LeaderBoardViewModel::class.java)

        binding.headerLayout.sortButtonLayout.setOnClickListener {
            bottomSheetFragment = SortByBottomSheet(this)
            val bundle = Bundle()
            bundle.putSerializable("STORE_LIST", list)
            bundle.putInt("SORT", sortClick)
            bottomSheetFragment?.arguments = bundle
            bottomSheetFragment?.show(supportFragmentManager, bottomSheetFragment?.tag)
        }

        // observer for store data
        leaderBoardViewModel.storeList.observe(this, Observer {
            list.addAll(it)
            leaderBoardItemsAdapter = LeaderBoardItemsAdapter(it, this)
            binding.rvLeaderBoardItems.adapter = leaderBoardItemsAdapter
            leaderBoardItemsAdapter.notifyDataSetChanged()
        })

        leaderBoardViewModel.loading.observe(this, Observer {
            binding.llProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        internetConnectionCheck()
    }

    private fun internetConnectionCheck() {
        if (!CorUtility.isNetworkAvailable(this)) {
            leaderBoardViewModel.loading.value = false
            CorUtility.showSnackbar(
                binding.leaderBoardLayoutRootContainer,
                getString(R.string.no_internet),
                getString(R.string.dismiss)
            )
        }
    }

    override fun onPause() {
        if (bottomSheetFragment != null && bottomSheetFragment?.isVisible!!)
            bottomSheetFragment?.dismiss()
        super.onPause()
    }

    /**
     * list item click listener
     */
    override fun <T> onItemClick(`object`: T, position: Int) {
        val bottomSheetFragment = MonthlySalesBottomSheet()
        val bundle = Bundle()
        if (`object` is Store) {
            bundle.putSerializable("STORE", `object`)
        }
        bottomSheetFragment.arguments = bundle
        bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
    }

    /**
     * Sort item selected
     */
    override fun onItemSelected(list: ArrayList<Store>) {
        sortClick = 1
        leaderBoardItemsAdapter = LeaderBoardItemsAdapter(list, this)
        binding.rvLeaderBoardItems.adapter = leaderBoardItemsAdapter
        leaderBoardItemsAdapter.notifyDataSetChanged()
    }
}